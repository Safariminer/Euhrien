#pragma once
#include <iostream>
#include <raylib.h>
#include <vector>
#include "Nadeo.h"

namespace Euhrien {
	namespace Graphics {
		class RenderingLayer { 
		public:
			virtual void LoadLayer() = 0;
			virtual void RenderLayer() = 0;
			virtual void UnloadLayer() = 0;
		};

		class _internal_Object3D {
		public:
			Model model;
			Vector3 position;
		};

		class _internal_awaiting_Object3D {
		public:
			std::string filename, shaderfilename;
			float x, y, z;
		};

		class RenderingLayer3D : public RenderingLayer{
			std::vector<_internal_Object3D> objects;
			std::vector<_internal_awaiting_Object3D> queue;
		public:

			int Add3DObject(std::string filename, std::string shaderfilename, float x, float y, float z); // before runtime
			void LoadLayer();
			int Load3DObject(std::string filename, std::string shaderfilename, float x, float y, float z); // at runtime
			void Kill3DObject(int object);
			void RenderLayer();
			void UnloadLayer();
		};



		class RenderingLayerML : public RenderingLayer {
			Font font;
			// Nadeo::CMlFrame frame;
			std::vector<Nadeo::CMlLabel> labels;
			Image image;
			Texture2D texture;
		public:
			void LoadXML(std::string filename);
			void LoadFont(std::string filename);
			void LoadLayer();
			void RenderLayer();
			void UnloadLayer();
		};
		class RenderingStack {
		public:
			std::vector<RenderingLayer*> layers;
			void LoadStack();
			void StackUpdate();
			void UnloadStack();
		};
	}
}