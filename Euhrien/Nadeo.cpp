#include "Nadeo.h"

#include <iostream>
#include <raylib.h>


Nadeo::CMlControl::CMlControl()
{
}

Nadeo::Void Nadeo::CMlControl::Render(Image& image, Font font)
{
    
}

Nadeo::CMlLabel::CMlLabel()
{
}

Nadeo::Void Nadeo::CMlLabel::Render(Image& image, Font font)
{
    int offset = 0;
    Vector2 textvector = MeasureTextEx(font, text.c_str(), textsize, 0);
    switch (HorizontalAlign) {
    
    case AlignHorizontal::Right:
        offset = -textvector.x;
        std::cout << "halign is right\n";
        break;
    case AlignHorizontal::HCenter:
        offset = -textvector.x / 2;
        std::cout << "halign is center\n";
        break;
    case AlignHorizontal::Left:
        std::cout << "halign is left\n";
        break;
    case AlignHorizontal::None:
        std::cout << "halign is none\n";
        break;
    }
    ImageDrawTextEx(&image,
        font,
        text.c_str(),
        { (float)(RelativePosition_V3.X+160)*4 + offset,
          (float)(-RelativePosition_V3.Y+90)*4 },
        textsize,
        0,
        WHITE
    );

    // std::cout << "X was : " << RelativePosition_V3.X << "; is now " << (RelativePosition_V3.X + 160) * 4 << "\n";
    // std::cout << "Y was : " << RelativePosition_V3.Y << "; is now " << (RelativePosition_V3.Y + 90) * 4 << "\n";


    /*
    DrawTextEx(
        font,
        text.c_str(),
        { (float)RelativePosition_V3.X + 160,
          (float)RelativePosition_V3.Y + 90 },
        10,
        0,
        WHITE
    );*/
    std::cout << "Rendered label\n";
}

Nadeo::CMlFrame::CMlFrame()
{
}

void Nadeo::CMlFrame::Render(Image& image, Font font)
{
    for (int i = 0; i < Controls.size(); i++)
        Controls.at(i)->Render(image, font);
}
