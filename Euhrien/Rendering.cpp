#include "Rendering.h"
#include <raylib.h>
#include "pugixml.hpp"
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include "Nadeo.h"

int Euhrien::Graphics::RenderingLayer3D::Add3DObject(std::string filename, std::string shaderfilename, float x, float y, float z)
{
	queue.push_back({ filename, shaderfilename, x, y, z });
	
	return queue.size()-1;
}

void Euhrien::Graphics::RenderingLayer3D::LoadLayer()
{
	std::cout << "Loaded RenderingLayer3D\n";
}

int Euhrien::Graphics::RenderingLayer3D::Load3DObject(std::string filename, std::string shaderfilename, float x, float y, float z)
{
	return 0;
}

void Euhrien::Graphics::RenderingLayer3D::Kill3DObject(int object)
{
}

void Euhrien::Graphics::RenderingLayer3D::RenderLayer()
{
	std::cout << "Rendered RenderingLayer3D\n";
}

void Euhrien::Graphics::RenderingLayer3D::UnloadLayer()
{
	std::cout << "Unloaded RenderingLayer3D\n";
}

void Euhrien::Graphics::RenderingLayerML::LoadXML(std::string filename)
{
	pugi::xml_document doc;
	bool loaded = doc.load_file(filename.c_str());
	if (loaded) {
		for (pugi::xml_node xmlLabel : doc.child("manialink").children("label")) {

			// std::vector<std::string> pos;
			Nadeo::CMlLabel newLabel;
			std::stringstream posstring(xmlLabel.attribute("pos").value());
			std::string segment;
			posstring >> segment;
			std::cout << segment << "\n";
			newLabel.RelativePosition_V3.X = (double)std::stoi(segment);
			std::cout << newLabel.RelativePosition_V3.X << "\n";
			segment = "";
			posstring >> segment;

			std::cout << segment << "\n";
			newLabel.RelativePosition_V3.Y = (double)std::stoi(segment);
			std::cout << newLabel.RelativePosition_V3.Y << "\n";
			newLabel.text = xmlLabel.attribute("text").value();
			newLabel.textsize = xmlLabel.attribute("textsize") ? std::stoi(xmlLabel.attribute("textsize").value()) : 30;
			newLabel.HorizontalAlign = Nadeo::CMlControl::AlignHorizontal::None;
			/*
			if (xmlLabel.attribute("halign")) {
				if ((std::string)xmlLabel.attribute("halign").as_string() == "center")
					newLabel.HorizontalAlign = Nadeo::CMlControl::AlignHorizontal::HCenter;
				if ((std::string)xmlLabel.attribute("halign").as_string() == "left")
					newLabel.HorizontalAlign = Nadeo::CMlControl::AlignHorizontal::Left;
				if ((std::string)xmlLabel.attribute("halign").as_string() == "right")
					newLabel.HorizontalAlign = Nadeo::CMlControl::AlignHorizontal::Right;
			}*/


			newLabel.HorizontalAlign =
				xmlLabel.attribute("halign") ? 
					(std::string)xmlLabel.attribute("halign").as_string() == "center" ?
						Nadeo::CMlControl::AlignHorizontal::HCenter :  
						(std::string)xmlLabel.attribute("halign").as_string() == "left" ?
							Nadeo::CMlControl::AlignHorizontal::Left :
							(std::string)xmlLabel.attribute("halign").as_string() == "right" ?
								Nadeo::CMlControl::AlignHorizontal::Right :
								Nadeo::CMlControl::AlignHorizontal::None
				: Nadeo::CMlControl::AlignHorizontal::None;
				
			labels.push_back(newLabel);
		}
		return (void)0;
	}
	else std::cout << "Can't load file " << filename << "\n";
}

void Euhrien::Graphics::RenderingLayerML::LoadFont(std::string filename)
{
	font = LoadFontEx(filename.c_str(), 100, 0, 360);
}

void Euhrien::Graphics::RenderingLayerML::LoadLayer()
{
	image = GenImageColor(1280, 720, BLANK);
	std::cout << "Loaded RenderingLayerML\n";
}

void Euhrien::Graphics::RenderingLayerML::RenderLayer()
{
	// frame.Render(image, font);
	ImageClearBackground(&image, BLANK);
	for (int i = 0; i < labels.size(); i++) labels.at(i).Render(image, font);
	
	UnloadTexture(texture);
	texture = LoadTextureFromImage(image);
	DrawTexturePro(texture, { 0, 0, 1280, 720 }, { 0, 0, (float)GetScreenWidth(), (float)GetScreenHeight() }, { 0, 0 }, 0, WHITE);
	// DrawTexture(texture, 0, 0, WHITE);
	

	// std::cout << "Rendered RenderingLayerML\n";
}

void Euhrien::Graphics::RenderingLayerML::UnloadLayer()
{
	std::cout << "Unloaded RenderingLayerML\n";
}

void Euhrien::Graphics::RenderingStack::LoadStack()
{
	for (int i = 0; i < layers.size(); i++) layers.at(i)->LoadLayer();
}

void Euhrien::Graphics::RenderingStack::StackUpdate()
{
	BeginDrawing();
	ClearBackground(BLUE);
	for (int i = 0; i < layers.size(); i++) layers.at(i)->RenderLayer();
	DrawFPS(0, 0);
	EndDrawing();
}

void Euhrien::Graphics::RenderingStack::UnloadStack()
{
	for (int i = 0; i < layers.size(); i++) layers.at(i)->UnloadLayer();
}