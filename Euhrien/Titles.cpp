#include "Titles.h"

#include <iostream>
#include <filesystem>
#include <raylib.h>

#include <vector>

std::string Euhrien::Titles::TitleManager()
{
	std::vector<std::string> titles;
	for (auto& p : std::filesystem::recursive_directory_iterator("Titles")) {
		if (p.is_directory()) {
			if (std::filesystem::exists(p.path().string() + "\\ManiaPlanetTitle.xml")){
				titles.push_back(p.path().string());
			}
		}
	}

	if (titles.size() == 1) return titles.at(0);

	InitWindow(640, 480, "Euhrien Title Manager(Pre-pre-pre-alpha 0.0)");
	SetTargetFPS(60);
	bool running = true;
	int selection = 0;
	while (running) {
		BeginDrawing();
		ClearBackground(BLACK);

		DrawRectangle(0, 10 * selection, GetScreenWidth(), 10, YELLOW);

		if (IsKeyPressed(KEY_DOWN) || IsKeyPressedRepeat(KEY_DOWN)) selection++;
		if (IsKeyPressed(KEY_UP) || IsKeyPressedRepeat(KEY_UP)) selection--;
		if (IsKeyPressed(KEY_ENTER) && selection < titles.size()) running = false;
		for (int i = 0; i < titles.size(); i++) {

			DrawText(titles.at(i).c_str(), 0, 10*i, 10, i == selection ? BLACK : WHITE);
		}
		EndDrawing();
	}
	CloseWindow();
	return titles.at(selection);
}
