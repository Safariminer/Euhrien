#pragma once
#include <iostream>
#include <vector>
#include <raylib.h>

namespace Nadeo{
	using Void = void;
	using Integer = int;
	using Real = double;
	using Boolean = bool;
	using Text = std::string;
	
	class Vec2 {
	public:
		Real X, Y;
	};
	
	class Vec3 : public Vec2 {
	public:
		Real Z;
	};
	
	class Int2 {
	public:
		Integer X, Y;
	};
	
	class Int3 : Int2 {
	public:
		Integer Z;
	};
	
	class Ident{};
	
	class CNod {
	public:
		Ident ident;
	};
	
	typedef enum {
		ExternalBrowser,
		ManialinkBrowser,
		Goto,
		ExternamFromId,
		ManialinkFromId,
		GotoFromId
	} LinkType;
	
	
	class CMlControl;
	
	class CMlFrame{
	public:
		CMlFrame();
		std::vector<CMlControl*> Controls;
		Font font;
		void Render(Image& image, Font font = GetFontDefault());
	};
	
	class CMlControl : public CNod {
	public:
		enum class AlignHorizontal
		{
			Left,
			HCenter,
			Right,
			None
		};
		enum class AlignVertical
		{
			Top,
			VCenter,
			Bottom,
			None,
			VCenter2
		};
		CMlControl();
		const CMlFrame Parent;
		const Text ControlId;
		std::vector<Text> ControlClasses;
		Vec2 Size;
		AlignHorizontal HorizontalAlign;
		AlignVertical VerticalAlign;
		Boolean Visible;
		Vec2 RelativePosition_V3;
		Real ZIndex, RelativeScale, RelativeRotation;
		Vec2 AbsolutePosition_V3;
		Real AbsoluteScale, AbsoluteRotation;
		Text ToolTip;
		Boolean IsFocused;
		Void Render(Image& image, Font font = GetFontDefault());
	};
	
	
	class CMlLabel : public CMlControl{
	public:
		CMlLabel();
		Text text;
		Void Render(Image& image, Font font = GetFontDefault());
		Integer textsize;
	};
}