#include <iostream>
#include <raylib.h>
#include "Rendering.h"
#include "Titles.h"
#include "pugixml.hpp"
#include <string>
#include <vector>

class Campaign {
public:
	std::vector<std::string> maps;
};

class ClassicMenu : public Euhrien::Graphics::RenderingLayer {
	std::string backgroundpath, headerpath;
	bool solo;
	std::vector<Campaign> campaigns;
	Font font;
	Texture2D background, header;
public:
	void SetBackground(std::string bgf);
	void SetHeader(std::string hf);
	void SetSolo(bool s);
	void LoadLayer();
	void RenderLayer();
	void UnloadLayer();
};

void ClassicMenu::SetBackground(std::string bgf) {
	backgroundpath = bgf;
}

void ClassicMenu::SetHeader(std::string hf) {
	headerpath = hf;
}

void ClassicMenu::SetSolo(bool s) {
	solo = s;
}

void ClassicMenu::LoadLayer()
{
	font = LoadFontEx("BaseData/fonts/CalSans-SemiBold.otf", 100, 0, 360);
	background = LoadTexture(backgroundpath.c_str());
	header = LoadTexture(headerpath.c_str());
}

void ClassicMenu::RenderLayer()
{
	DrawTextureEx(background, { 0, 0 }, 0, GetScreenWidth() > GetScreenHeight() ? background.width / GetScreenWidth() : background.height / GetScreenHeight(), WHITE);
	DrawTextureEx(header, { 0, 0 }, 0, 0.5f, WHITE);
	DrawTextEx(font, "Solo", { 10, 128 }, 30, 0, solo ? WHITE : GRAY);
	DrawTextEx(font, "Quit", { 10, 158 }, 30, 0, WHITE);
	if (GetMouseX() > 10 && GetMouseY() < 158 && GetMouseY() > 128){
		DrawTextEx(font, "Solo", { 10, 128 }, 30, 0, solo ? GOLD : IsMouseButtonDown(MOUSE_BUTTON_LEFT) ? RED: WHITE);
		
	}
	if (GetMouseX() > 10 && GetMouseY() < 188 && GetMouseY() > 158) {
		DrawTextEx(font, "Quit", { 10, 158 }, 30, 0, GOLD);
		if (IsMouseButtonPressed(MOUSE_BUTTON_LEFT)) exit(0);
	}
}

void ClassicMenu::UnloadLayer()
{
	UnloadFont(font);
}


int main() {

	// InitWindow(1280, 720, "Euhrien");

	std::string title = Euhrien::Titles::TitleManager();

	pugi::xml_document doc;
	if (!doc.load_file((title + "/ManiaPlanetTitle.xml").c_str())) {
		std::cout << "Couldn't load ManiaPlanetTitle.xml";
	}
	SetConfigFlags(FLAG_MSAA_4X_HINT);
	SetConfigFlags(FLAG_WINDOW_RESIZABLE);

	InitWindow(1280, 720, (((std::string)doc.child("maniaplanet_title").child_value("name")) + " - Euhrien Engine").c_str());
	SetTargetFPS(60);
	ClassicMenu menu;
	Euhrien::Graphics::RenderingLayerML menuMl;

	Euhrien::Graphics::RenderingStack rStack;
	std::string manialinkstring = doc.child("maniaplanet_title").child("menu").child("manialink").attribute("url").as_string();
	if (manialinkstring == "") {
		menu.SetBackground(title + "\\" + doc.child("maniaplanet_title").child("menu").child("background_img").attribute("name").value());
		menu.SetHeader(title + "\\" + doc.child("maniaplanet_title").child("menu").child("header").attribute("name").value());
		menu.SetSolo(doc.child("maniaplanet_title").child("solo"));
		rStack.layers.push_back(&menu);
	}
	else if (!(manialinkstring.ends_with(".xml"))) {
		std::cout << "TODO: ManiaScript menus are not yet supported.\n";
		menu.SetBackground(title + "\\" + doc.child("maniaplanet_title").child("menu").child("background_img").attribute("name").value());
		menu.SetHeader(title + "\\" + doc.child("maniaplanet_title").child("menu").child("header").attribute("name").value());
		menu.SetSolo(doc.child("maniaplanet_title").child("solo"));
		rStack.layers.push_back(&menu);
	}
	else {
		menuMl.LoadXML(title + "\\" + manialinkstring);
		// std::cout << title + "\\" + (std::string)doc.child("maniaplanet_title").child("menu").child("manialink").attribute("name").as_string() << "\n";
		menuMl.LoadFont(doc.child("maniaplanet_title").child("menu").child("font") ? title + "\\" + doc.child("maniaplanet_title").child("menu").child("font").attribute("name").value() : "C:\\Windows\\Fonts\\Arial.ttf");
		rStack.layers.push_back(&menuMl);
	}
	
	

	SetExitKey(0);
	rStack.LoadStack();

	while (!WindowShouldClose()) {
		rStack.StackUpdate();
	}
	
	rStack.UnloadStack();

	/*
	Euhrien::Graphics::RenderingStack menu_rStack;
	Euhrien::Graphics::RenderingLayer3D menu_rLayer3D;
	Euhrien::Graphics::RenderingLayerML menu_rLayerML;
	menu_rStack.layers.push_back(&menu_rLayer3D);
	menu_rStack.layers.push_back(&menu_rLayerML);

	menu_rStack.LoadStack();
	menu_rStack.StackUpdate();
	menu_rStack.UnloadStack();
	*/

}
