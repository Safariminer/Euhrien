# Euhrien
## An open-source Gamebox reimplementation/reinterpretation in C++

Euhrien is a game engine heavily inspired by Gamebox, aiming to be fully compatible with it, whilst adding original features on top of it, like an XML-based map format.

As of now, it can't do much, but it can read from a ManiaPlanetTitle.xml file to load a menu background image and header image file and load labels from a ManiaLink.

## Why Euhrien?
[Mostly to get my revenge from that time Gabiola2/QuantumDeathCat attempted a cut in Lan ETS 2024 and he was called out for it and he said "nonono, look, that's okay, look, even Safari is catching up to me".](https://www.youtube.com/watch?v=1Jq15IiYVIQ) Yes I am a bit petty.

## Oh my god, I need to try this out
[Check the releases!](https://gitlab.com/Safariminer/Euhrien/-/releases) There should be one ready for you to try out right now! (Windows only though)

You might need the [redists for MSVC](https://learn.microsoft.com/en-us/cpp/windows/latest-supported-vc-redist?view=msvc-170#visual-studio-2015-2017-2019-and-2022).

Once you launch Euhrien.exe, you will be faced with a title manager. Use Up and Down to navigate it and press enter to select your title.

There are two example titles:
- EuhrienGameSample_ClassicMenus: a demo of default menus in Euhrien
- EuhrienGameSample_Xml: a demo of the ManiaLink engine, complete with Gamebox's signature UI scaling we all love so much.

## How it works
It works by stacking rendering layers on top of one another. You can create a custom rendering layer by deriving it from the Euhrien::Graphics::RenderingLayer class and then push it onto a rendering stack(Euhrien::Graphics::RenderingStack).

## Some notes:
- I am currently learning how to use GitLab and moving away from Github for productivity.
- I am currently working alone on this project, thus progress is slow and so is the road to compatibility.
- This is still in extremely early development. 

## Compatibility:
- Maps: no
- [ManiaScript: not nearly enough](https://github.com/Safariminer/msrt)
- ManiaLinks: not yet but it's getting there. (Labels function though, see EuhrienGameSample_Xml)

## Features:
``eww no wtf is that?`` More seriously, it's going to take a while to at least get to the level of [my previous engine](https://github.com/Safariminer/tvgame).

## Can I use this to make games myself?
Yes. Although it definitely resembles Gamebox a lot(even in its code; if Nadeo and I got something in common, it's our love for spaghetti code <3), it uses 0% Nadeo code. But, I currently can't guarantee that it'll have all of the features you want from an engine or even that it works well.

## Dependencies:
- raylib
- pugixml